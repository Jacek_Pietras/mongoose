/*jshint node: true*/
var express = require("express");
var app = express();
var port = process.env.PORT || 8000;

var cookieParser = require("cookie-parser");
var session = require("express-session");
var morgan = require("morgan");
var mongoose = require("mongoose");

var configDB = require("./db/database.js");
mongoose.connect(configDB.url);

app.use(express.static("public"));
app.use(morgan("dev"));
app.use(cookieParser());
app.use(session({
    secret: "wszystkoOK",
    saveUninitialized: true,
    resave: true
}));

//app.use("/", function (req, res) {
//    res.send("Program w expressie");
//    console.dir(req.cookies);
//    console.dir("================");
//    console.dir(req.session);
//});

require("./app/routes.js")(app);

app.listen(port);
console.dir("Serwer działa na porcie: " + port);