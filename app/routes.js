/*jshint node: true*/

var User = require("./modules/user");

module.exports = function (app) {
    app.get("/", function (req, res) {
        res.send("Witam, co tam?");
    });

    app.get("/:username/:password", function (req, res) {
        var newUser = new User();
        newUser.local.username = req.params.username;
        newUser.local.password = req.params.password;

        console.dir(newUser.local.username + " " + newUser.local.password);

        newUser.save(function (err) {
            if (err) {
                throw err;
            }
        });
        res.send("Sukces!");
    });
};